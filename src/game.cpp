#include "game.h"

namespace ENDLESS
{
	Game::Game()
	{
		windowsWidth = 1280;
		windowsHeight = 720;
		fps = 60;
		exit = false;
		startB = NULL;
		creditsB = NULL;
		exitB = NULL;
		sceneManager = NULL;
		acceptExitB = NULL;
		deniedExitB = NULL;
		resumeGameB = NULL;
		resetGameB = NULL;
		goMenuB = NULL;
		for (short i = 0; i < max_lanes; i++) 
		{ 
			lanes[i] = NULL; 
		}
		player = NULL;
		spawner = NULL;			
		scrollingBack = 0.0f;
		scrollingMid = 0.0f;
		scrollingFore = 0.0f;		
	}
	Game::~Game()
	{
		if (startB != NULL) { delete startB; startB = NULL; }
		if (creditsB != NULL) { delete creditsB; creditsB = NULL; }
		if (exitB != NULL) { delete exitB; exitB = NULL; }
		if (sceneManager != NULL) { delete sceneManager; sceneManager = NULL; }
		if (acceptExitB != NULL) { delete acceptExitB; acceptExitB = NULL; }
		if (deniedExitB != NULL) { delete deniedExitB; deniedExitB = NULL; }
		if (resumeGameB != NULL) { delete resumeGameB; resumeGameB = NULL; };
		if (resetGameB != NULL) { delete resetGameB; resetGameB = NULL; };
		if (goMenuB != NULL) { delete goMenuB; goMenuB = NULL; };
		for (short i = 0; i < max_lanes; i++)
		{
			if (lanes[i] != NULL) { delete lanes[i]; lanes[i] = NULL; }
		}
		if (player != NULL) { delete player; player = NULL; }
		if (spawner != NULL) { delete spawner; spawner = NULL; }
	}
	void Game::Init()
	{
		InitWindow(windowsWidth, windowsHeight, "ENDLESS v0.1");

		background = LoadTexture("../res/assets/textures/back.png");
		midground = LoadTexture("../res/assets/textures/middle.png");
		midground2 = LoadTexture("../res/assets/textures/secondmiddle.png");
		foreground = LoadTexture("../res/assets/textures/front.png");	
		heartTexture = LoadTexture("../res/assets/textures/heart.png");
		heartTexture.width = 50;
		heartTexture.height = 50;
		menuesBackground = LoadTexture("../res/assets/textures/back.png");
		menuesBackground.width = GetScreenWidth();
		menuesBackground.height = GetScreenHeight();
		obstacleOneTxtr = LoadTexture("../res/assets/textures/obs1.png");
		obstacleTwoTxtr = LoadTexture("../res/assets/textures/obs2.png");
		playerTxtr = LoadTexture("../res/assets/textures/player.png");
		introSong = LoadMusicStream("../res/assets/music/intro.mp3");
		gameSong = LoadMusicStream("../res/assets/music/gameMusic.mp3");
		SetMusicVolume(introSong, 0.35f);
		SetMusicVolume(gameSong, 0.35f);

		startB = new Button(
			(windowsWidth / 2) - ((windowsWidth / 5) / 2),
			((windowsHeight / 4) + (windowsHeight / 9) * 0),
			windowsWidth / 5,
			windowsHeight / 9,
			"START",
			(windowsWidth / 2) - ((windowsWidth / 5) / 2) + ((30 * 5) / 4),
			((windowsHeight / 4) + (windowsHeight / 9) * 0) + ((30 * 5) / 10),
			(windowsHeight / 25));

		creditsB = new Button(
			(windowsWidth / 2) - ((windowsWidth / 5) / 2),
			((windowsHeight / 4) + (windowsHeight / 9) * 2),
			windowsWidth / 5,
			windowsHeight / 9,
			"CREDITS",
			(windowsWidth / 2) - ((windowsWidth / 5) / 2) + ((30 * 6) / 4),
			((windowsHeight / 4) + (windowsHeight / 9) * 2) + ((30 * 6) / 10),
			(windowsHeight / 25));

		exitB = new Button(
			(windowsWidth / 2) - ((windowsWidth / 5) / 2),
			((windowsHeight / 4) + (windowsHeight / 9) * 4),
			windowsWidth / 5,
			windowsHeight / 9,
			"EXIT",
			(windowsWidth / 2) - ((windowsWidth / 5) / 2) + ((30 * 4) / 4),
			((windowsHeight / 4) + (windowsHeight / 9) * 4) + ((30 * 4) / 10),
			(windowsHeight / 25));

		acceptExitB = new Button(
			(windowsWidth / 2) - ((windowsWidth / 5) / 2),
			((windowsHeight / 4) + (windowsHeight / 9) * 0),
			windowsWidth / 5,
			windowsHeight / 9,
			"YES",
			(windowsWidth / 2) - ((windowsWidth / 5) / 2) + ((30 * 3) / 4),
			((windowsHeight / 4) + (windowsHeight / 9) * 0) + ((30 * 3) / 10),
			(windowsHeight / 25));

		deniedExitB = new Button(
			(windowsWidth / 2) - ((windowsWidth / 5) / 2),
			((windowsHeight / 4) + (windowsHeight / 9) * 2),
			windowsWidth / 5,
			windowsHeight / 9,
			"NO",
			(windowsWidth / 2) - ((windowsWidth / 5) / 2) + ((30 * 2) / 4),
			((windowsHeight / 4) + (windowsHeight / 9) * 2) + ((30 * 2) / 10),
			(windowsHeight / 25));

		resumeGameB = new Button(
			(windowsWidth / 2) - ((windowsWidth / 5) / 2),
			((windowsHeight / 4) + (windowsHeight / 9) * 0),
			windowsWidth / 5,
			windowsHeight / 9,
			"RESUME",
			(windowsWidth / 2) - ((windowsWidth / 5) / 2) + ((30 * 5) / 4),
			((windowsHeight / 4) + (windowsHeight / 9) * 0) + ((30 * 5) / 10),
			(windowsHeight / 25));

		resetGameB = new Button(
			(windowsWidth / 2) - ((windowsWidth / 5) / 2),
			((windowsHeight / 4) + (windowsHeight / 9) * 2),
			windowsWidth / 5,
			windowsHeight / 9,
			"RESET",
			(windowsWidth / 2) - ((windowsWidth / 5) / 2) + ((30 * 6) / 4),
			((windowsHeight / 4) + (windowsHeight / 9) * 2) + ((30 * 6) / 10),
			(windowsHeight / 25));

		goMenuB = new Button(
			(windowsWidth / 2) - ((windowsWidth / 5) / 2),
			((windowsHeight / 4) + (windowsHeight / 9) * 4),
			windowsWidth / 5,
			windowsHeight / 9,
			"MENU",
			(windowsWidth / 2) - ((windowsWidth / 5) / 2) + ((30 * 4) / 4),
			((windowsHeight / 4) + (windowsHeight / 9) * 4) + ((30 * 4) / 10),
			(windowsHeight / 25));

		sceneManager = new Scenes(MainScenes::menu, BLACK);

		for (short i = 0; i < max_lanes; i++)
		{
			lanes[i] = new Lane();
			lanes[i]->setWidth(windowsWidth);
			lanes[i]->setHeight((windowsHeight * 10) / 100);
			lanes[i]->setX(0);
			lanes[i]->setY((windowsHeight - (lanes[i]->getHeight() * (i + 1))) - (((windowsHeight * 10) / 100) * i));
			lanes[i]->setColor(GRAY);
		}

		player = new Player();
		spawner = new Spawner();

		InitAudioDevice();
		SetTargetFPS(fps);
	}
	void Game::Input()
	{
		switch (sceneManager->GetCurrentScene())
		{
		case MainScenes::menu:
			sceneManager->ChangeCurrentScene(startB->ChangeSceneOnClick(sceneManager->GetCurrentScene(), MainScenes::game));
			sceneManager->ChangeCurrentScene(creditsB->ChangeSceneOnClick(sceneManager->GetCurrentScene(), MainScenes::credits));
			sceneManager->ChangeCurrentScene(exitB->ChangeSceneOnClick(sceneManager->GetCurrentScene(), MainScenes::warningExit));
			break;
		case MainScenes::game:
			player->Input(sceneManager);
			break;
		case MainScenes::credits:
			if (IsKeyPressed(KEY_ENTER))
			{
				sceneManager->ChangeCurrentScene(MainScenes::menu);				
			}
			break;
		case MainScenes::warningExit:
			sceneManager->ChangeCurrentScene(acceptExitB->ChangeSceneOnClick(sceneManager->GetCurrentScene(), MainScenes::exit));
			sceneManager->ChangeCurrentScene(deniedExitB->ChangeSceneOnClick(sceneManager->GetCurrentScene(), MainScenes::menu));
			break;
		case MainScenes::pause:
			sceneManager->ChangeCurrentScene(resumeGameB->ChangeSceneOnClick(sceneManager->GetCurrentScene(), MainScenes::game));
			resetGameB->ResetGameOnClick(sceneManager, spawner, player, MainScenes::game, gameSong);
			goMenuB->ResetGameOnClick(sceneManager, spawner, player, MainScenes::menu, gameSong);
			break;

		case MainScenes::gameOver:
			if (IsKeyPressed(KEY_ENTER))
			{
				sceneManager->ChangeCurrentScene(MainScenes::menu);
				player->Reset();
				spawner->Reset();
			}
			break;
		default:
			break;
		}
	}
	void Game::Update()
	{
		switch (sceneManager->GetCurrentScene())
		{
		case MainScenes::menu:

			if(IsMusicPlaying(gameSong))
			{
				StopMusicStream(gameSong);
			}

			if(!IsMusicPlaying(introSong))
			{
				PlayMusicStream(introSong);
			}

			UpdateMusicStream(introSong);

			break;
		case MainScenes::game:		
			
			if (IsMusicPlaying(introSong))
			{
				StopMusicStream(introSong);
			}

			if(!IsMusicPlaying(gameSong))
			{
				PlayMusicStream(gameSong);
			}

			UpdateMusicStream(gameSong);			

			scrollingBack -= 0.1f;
			scrollingMid -= 0.5f;
			scrollingFore -= 1.0f;

			if (scrollingBack <= -background.width * 2) scrollingBack = 0;
			if (scrollingMid <= -midground.width * 2) scrollingMid = 0;
			if (scrollingFore <= -foreground.width * 2) scrollingFore = 0;

			player->Update((lanes[2]->getY() + (lanes[2]->getHeight() / 4)),
				(lanes[1]->getY() + (lanes[1]->getHeight() / 4)),
				(lanes[0]->getY() + (lanes[0]->getHeight() / 4)));
			player->GameOver(sceneManager);

			spawner->Update(0, lanes[2]->getY(), lanes[1]->getY(), lanes[0]->getY());
			spawner->Colisiones(player);
			break;
		case MainScenes::credits:
			UpdateMusicStream(introSong);
			break;
		case MainScenes::warningExit:
			UpdateMusicStream(introSong);
			break;
		case MainScenes::pause:
			PauseMusicStream(gameSong);
			break;
		case MainScenes::exit:
			exit = true;
			break;
		default:
			break;
		}
	}
	void Game::Draw()
	{
		switch (sceneManager->GetCurrentScene())
		{
		case MainScenes::menu:

			BeginDrawing();

			ClearBackground(sceneManager->GetBackgroundColor());	

			DrawTexture(menuesBackground,1,1,WHITE);
			DrawText("ENDLESS RUNNER", (windowsWidth / 2) - ((25 * 15) / 2), 50, 40, GREEN);
			DrawText("v1.0", windowsHeight - (5 * 30), windowsHeight - 35, 30, GREEN);

			startB->DrawButton();
			creditsB->DrawButton();
			exitB->DrawButton();			

			EndDrawing();

			break;
		case MainScenes::game:

			BeginDrawing();

			ClearBackground(sceneManager->GetBackgroundColor());

			DrawTextureEx(background, { scrollingBack, 20 }, 0.0f, 5.0f, WHITE);//2.0f
			DrawTextureEx(background, { background.width * 2 + scrollingBack, 20 }, 0.0f, 5.0f, WHITE);


			DrawTextureEx(midground, { scrollingMid, 20 }, 0.0f, 5.0f, WHITE);
			DrawTextureEx(midground, { midground.width * 2 + scrollingMid, 20 }, 0.0f, 5.0f, WHITE);

			DrawTextureEx(midground2, { scrollingMid, 20 }, 0.0f, 5.0f, WHITE);
			DrawTextureEx(midground2, { midground2.width * 2 + scrollingMid, 20 }, 0.0f, 5.0f, WHITE);


			DrawTextureEx(foreground, { scrollingFore, 70 }, 0.0f, 6.0f, WHITE);
			DrawTextureEx(foreground, { foreground.width * 2 + scrollingFore, 70 }, 0.0f, 6.0f, WHITE);			

			for (short i = 0; i < max_lanes; i++)
			{
				lanes[i]->Draw();
			}

			player->Draw(playerTxtr);
			player->DrawLifes(heartTexture);
			spawner->Draw(obstacleOneTxtr, obstacleTwoTxtr);

			DrawText("UP KEYPAD (UP)", 10, 50, 25, WHITE);
			DrawText("DOWN KEYPAD (DOWN)", 10, 100, 25, WHITE);
			DrawText("SPACE (JUMP)", 10, 150, 25, WHITE);
			DrawText("P (PAUSE)", 10, 200, 25, WHITE);

			EndDrawing();			

			break;
		case MainScenes::credits:

			BeginDrawing();

			ClearBackground(sceneManager->GetBackgroundColor());

			DrawTexture(menuesBackground, 1, 1, WHITE);
			DrawText("ENDLESS RUNNER", (windowsWidth / 2) - ((25 * 15) / 2), 50, 40, GREEN);
			DrawText("v1.0", windowsHeight - (5 * 30), windowsHeight - 35, 30, GREEN);
			DrawText("CREDITS", (GetScreenWidth()/2) - (7 * 30), 150, 30, GREEN);
			DrawText("GAME PROGRAMED BY FACUNDO WILLIAMS", 20, 250, 30, GREEN);
			DrawText("ASSETS:", 20, 300, 30, GREEN);
			DrawText("https://opengameart.org/content/industrial-parallax-background", 20, 340, 30, GREEN);
			DrawText("https://opengameart.org/content/heart-pixel-art", 20, 380, 30, GREEN);
			DrawText("https://opengameart.org/content/isometric-tile-set", 20, 420, 30, GREEN);
			DrawText("https://opengameart.org/content/soldier-walking-animation", 20, 460, 30, GREEN);
			DrawText("https://opengameart.org/content/battle-theme-a", 20, 500, 30, GREEN);
			DrawText("https://www.instagram.com/santisega/", 20, 540, 30, GREEN);
			
			DrawText("PRESS ENTER TO GO TO MENU", 20, 580, 30, GREEN);

			EndDrawing();

			break;
		case MainScenes::warningExit:
			BeginDrawing();

			ClearBackground(sceneManager->GetBackgroundColor());

			DrawTexture(menuesBackground, 1, 1, WHITE);
			DrawText("Do you want to exit game?", (windowsWidth / 2) - ((25 * 30) / 4), (windowsHeight / 10), 30, GREEN);
			acceptExitB->DrawButton();
			deniedExitB->DrawButton();

			EndDrawing();

			break;
		case MainScenes::pause:

			BeginDrawing();

			ClearBackground(sceneManager->GetBackgroundColor());

			DrawTexture(menuesBackground, 1, 1, WHITE);
			DrawText("GAME PAUSED", (windowsWidth / 2) - ((17 * 30) / 4), (windowsHeight / 10), 35, GREEN);
			resumeGameB->DrawButton();
			resetGameB->DrawButton();
			goMenuB->DrawButton();

			EndDrawing();

			break;
		case MainScenes::gameOver:

			BeginDrawing();

			ClearBackground(sceneManager->GetBackgroundColor());

			DrawTexture(menuesBackground, 1, 1, WHITE);
			DrawText("GAME OVER!", (windowsWidth / 2) - (10 * 35) / 2, windowsHeight / 2, 65, RED);
			DrawText("PRESS ENTER TO GO TO MENU", (windowsWidth / 2) - (36 * 35) / 2, 500, 40, GREEN);
			
			EndDrawing();

			break;
		default:
			break;
		}
	}
	void Game::Deinit()
	{
		UnloadTexture(background);
		UnloadTexture(midground);
		UnloadTexture(midground2);
		UnloadTexture(foreground);		
		UnloadTexture(heartTexture);
		UnloadTexture(menuesBackground);
		UnloadTexture(obstacleOneTxtr);
		UnloadTexture(obstacleTwoTxtr);
		UnloadTexture(playerTxtr);
		UnloadMusicStream(introSong);
		UnloadMusicStream(gameSong);

		CloseAudioDevice();

		CloseWindow();
	}
	void Game::Play()
	{
		Init();

		while (!WindowShouldClose() && !exit)
		{
			Input();
			Update();
			Draw();
		}

		Deinit();
	}
}

#ifndef SCENES_H

#define SCENES_H

#include "raylib.h"

namespace ENDLESS
{
	enum class MainScenes { menu, game, credits, warningExit, exit, pause, gameOver };

	class Scenes
	{
	private:		
		MainScenes currentScene;
		Color background;
	public:
		Scenes();
		Scenes(MainScenes scene, Color color);		
		void ChangeCurrentScene(MainScenes scene);
		MainScenes GetCurrentScene();
		void SetBackgroundColor(Color color);
		Color GetBackgroundColor();		
	};
}

#endif

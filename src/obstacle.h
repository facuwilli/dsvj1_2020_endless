#ifndef OBSTACLE_H

#define OBSTACLE_H
#include "raylib.h"

namespace ENDLESS 
{
	enum class ObstaclesType { laser, drop };

	class Obstacle
	{
	private:
		Rectangle rec;
		Color color;
		float speed;
		ObstaclesType type;
		bool active;
	public:
		Obstacle();
		void SetRectangle(float x, float y, float width, float height);
		void SetColor(Color _color);
		void SetType(ObstaclesType _type);
		void SetActive(bool status);
		void SetSpeed(float _speed);
		void AddSpeed(float value);
		Rectangle GetRec();
		float GetY();		
		float GetX();
		float GetWidth();
		bool GetActive();
		float GetSpeed();
		ObstaclesType GetType();
		void Move();
		void Update(int limit);
		void Draw(Texture2D txtrOne, Texture2D txtrTwo);		
	};
}
#endif

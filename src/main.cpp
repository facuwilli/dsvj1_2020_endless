#include "game.h"

using namespace ENDLESS;

int main()
{
	Game* endless = new Game();

	endless->Play();

	delete endless;

	return 0;
}
#ifndef LANE_H

#define LANE_H

#include "raylib.h"

namespace ENDLESS
{
	class Lane
	{
	private:
		Rectangle rec;
		Color color;
	public:
		Lane();
		void setX(float x);
		void setY(float y);
		void setWidth(float width);
		void setHeight(float height);
		void setColor(Color _color);
		float getHeight();
		float getY();
		void Draw();
	};
}
#endif 

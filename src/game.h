#ifndef GAME_H

#define GAME_H

#include "raylib.h"
#include "button.h"
#include "scenes.h"
#include "lane.h"
#include "player.h"
#include "spawner.h"

namespace ENDLESS
{
	const short max_lanes = 3;	

	class Game
	{
	private:
		int windowsWidth;
		int windowsHeight;
		int fps;
		bool exit;
		Button* startB;
		Button* exitB;
		Button* creditsB;
		Button* acceptExitB;
		Button* deniedExitB;
		Button* resumeGameB;
		Button* resetGameB;
		Button* goMenuB;
		Scenes* sceneManager;	
		Lane* lanes[max_lanes];
		Player* player;
		Spawner* spawner;
		Texture2D background;
		Texture2D midground;
		Texture2D midground2;
		Texture2D foreground;		
		float scrollingBack;
		float scrollingMid;
		float scrollingFore;	
		Texture2D heartTexture;
		Texture2D menuesBackground;	
		Texture2D obstacleOneTxtr;
		Texture2D obstacleTwoTxtr;
		Texture2D playerTxtr;
		Music introSong;
		Music gameSong;
	public:
		Game();
		~Game();
		void Init();
		void Input();
		void Update();
		void Draw();
		void Deinit();
		void Play();
	};	
}

#endif

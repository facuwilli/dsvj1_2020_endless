#include "spawner.h"
namespace ENDLESS
{
	Spawner::Spawner()
	{
		timer = 0;
		minTime = 90;
		maxTime = 170;
		newObstacleTime = GetFrameTime() * static_cast<float>(GetRandomValue(minTime, maxTime));
		laneYPos = 0;
		index = 0;
		for (short i = 0; i < numObstacles; i++)
		{
			obstacles[i] = new Obstacle();
		}
		lastLane = 0;
		currentLane = 0;
		speedTimer = 0;
		timeTillSpeedUp = GetRandomValue(4, 8);
		maxTimesSpeedUp = 20;
	}
	Spawner::~Spawner()
	{
		for (short i = 0; i < numObstacles; i++)
		{
			if (obstacles[i] != NULL)
			{
				delete obstacles[i];
			}
		}
	}
	void Spawner::ObstaclesGenerator(float firstLaneY, float secondLaneY, float thirdLaneY)
	{
		timer += GetFrameTime();

		if (timer >= newObstacleTime)
		{
			obstacles[index]->SetType(ReturnRandomType());

			RandomLane();

			switch (currentLane)
			{
			case 1:
				SetUpObstacle(firstLaneY);
				break;
			case 2:
				SetUpObstacle(secondLaneY);
				break;
			case 3:
				SetUpObstacle(thirdLaneY);
				break;			
			}
			
			timer = 0;
			speedTimer += 1;
			newObstacleTime = GetFrameTime() * static_cast<float>(GetRandomValue(minTime, maxTime));
			index++;
		}

		if(index >= 49)
		{
			index = 0;
		}
	}
	ObstaclesType Spawner::ReturnRandomType()
	{
		ObstaclesType type = ObstaclesType::laser;
		short num = GetRandomValue(1, 5);

		if (num == 3)
		{
			type = ObstaclesType::drop;
		}
		else
		{
			type = ObstaclesType::laser;
		}

		return type;
	}
	void Spawner::SetUpObstacle(float laneYPos)
	{
		obstacles[index]->SetActive(true);

		switch (obstacles[index]->GetType())
		{
		case ObstaclesType::laser:
			obstacles[index]->SetRectangle(GetScreenWidth(), laneYPos + (((GetScreenHeight() / 10) * 25) / 100), GetScreenWidth() / 35, (((GetScreenHeight() / 10) * 75) / 100));
			obstacles[index]->SetColor(BLUE);
			break;
		case ObstaclesType::drop:
			obstacles[index]->SetRectangle(GetScreenWidth(), laneYPos, GetScreenWidth() / 4, GetScreenHeight() / 10);
			obstacles[index]->SetColor(YELLOW);
			break;
		}
	}
	void Spawner::RandomLane()
	{
		currentLane = GetRandomValue(1, 3);

		if (currentLane == lastLane)
		{
			if (currentLane == 1)
			{
				currentLane += 1;
			}
			else
			{
				currentLane -= 1;
			}
		}

		lastLane = currentLane;		
	}
	void Spawner::Update(int limit, float firstLaneY, float secondLaneY, float thirdLaneY)
	{
		SpeedUp(); 

		ObstaclesGenerator(firstLaneY, secondLaneY, thirdLaneY);

		for (short i = 0; i < numObstacles; i++)
		{
			obstacles[i]->Update(limit);
		}
	}
	void Spawner::Draw(Texture2D txtrOne, Texture2D txtrTwo)
	{
		for (short i = 0; i < numObstacles; i++)
		{
			obstacles[i]->Draw(txtrOne, txtrTwo);
		}
	}
	void Spawner::Colisiones(Player* player)
	{
		for (short i = 0; i < numObstacles; i++) 
		{
			if (CheckCollisionRecs(player->GetRec(), obstacles[i]->GetRec()) && obstacles[i]->GetActive())
			{
				obstacles[i]->SetActive(false);
				player->SetLifes(player->GetLifes() - 1);
				i = numObstacles;
			}
			else if ((player->GetY() > (obstacles[i]->GetY() - player->GetJumpHeight() - 1)) && (player->GetY() < obstacles[i]->GetY())
					&& player->GetX() < (obstacles[i]->GetX() + obstacles[i]->GetWidth()) && (player->GetX() + player->GetWidth()) > obstacles[i]->GetX())
			{
				player->AddScore(7, 1);
			}
		}		
	}
	void Spawner::Reset()
	{
		timer = 0;
		minTime = 90;
		maxTime = 170;
		newObstacleTime = GetFrameTime() * static_cast<float>(GetRandomValue(minTime, maxTime));
		index = 0;
		lastLane = 0;
		for (short i = 0; i < numObstacles; i++)
		{
			obstacles[i]->SetSpeed(175);

			if(obstacles[i]->GetActive())
			{
				obstacles[i]->SetActive(false);				
			}			
		}
		speedTimer = 0;
		timeTillSpeedUp = GetRandomValue(4, 8);
		maxTimesSpeedUp = 20;		
	}
	void Spawner::SpeedUp()
	{
		if(speedTimer >= timeTillSpeedUp && maxTimesSpeedUp > 0)
		{
			speedTimer = 0;
			timeTillSpeedUp = GetRandomValue(4, 8); 	
			minTime -= 2;
			maxTime -= 4;
			for (short i = 0; i < numObstacles; i++)
			{
				obstacles[i]->AddSpeed(25);
			}
			maxTimesSpeedUp -= 1;
		}
	}
}
#include "scenes.h"

namespace ENDLESS
{
	Scenes::Scenes()
	{
		currentScene = MainScenes::menu;
		background = BLACK;
	}
	Scenes::Scenes(MainScenes scene, Color color)
	{
		currentScene = scene;
		background = color;
	}	
	void Scenes::ChangeCurrentScene(MainScenes scene)
	{
		currentScene = scene;
	}
	MainScenes Scenes::GetCurrentScene()
	{
		return currentScene;
	}
	void Scenes::SetBackgroundColor(Color color)
	{
		background = color;
	}
	Color Scenes::GetBackgroundColor()
	{
		return background;
	}	
}
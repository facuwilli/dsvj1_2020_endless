#include "obstacle.h"

namespace ENDLESS 
{
	Obstacle::Obstacle()
	{
		rec.x = 0;
		rec.y = 0;
		rec.width = 0;
		rec.height = 0;
		color = WHITE;
		speed = 175;
		type = ObstaclesType::laser;
		active = false;
	}
	void Obstacle::SetRectangle(float x, float y, float width, float height)
	{
		rec.x = x;
		rec.y = y;
		rec.width = width;
		rec.height = height;
	}
	void Obstacle::SetColor(Color _color)
	{
		color = _color;
	}
	void Obstacle::SetType(ObstaclesType _type)
	{
		type = _type;
	}
	void Obstacle::SetActive(bool status)
	{
		active = status;
	}
	void Obstacle::SetSpeed(float _speed)
	{
		speed = _speed;
	}
	void Obstacle::AddSpeed(float value)
	{
		speed += value;
	}
	Rectangle Obstacle::GetRec()
	{
		return rec;
	}
	float Obstacle::GetY()
	{
		return rec.y;
	}
	float Obstacle::GetX()
	{
		return rec.x;
	}
	float Obstacle::GetWidth()
	{
		return rec.width;
	}
	bool Obstacle::GetActive()
	{
		return active;
	}
	float Obstacle::GetSpeed()
	{
		return speed;
	}
	ObstaclesType Obstacle::GetType()
	{
		return type;
	}
	void Obstacle::Move()
	{
		if (active)
		{
			rec.x -= GetFrameTime() * speed;
		}
	}
	void Obstacle::Update(int limit)
	{
		if (rec.x <= limit - rec.width)
		{
			SetActive(false);
		}

		Move();
	}
	void Obstacle::Draw(Texture2D txtrOne, Texture2D txtrTwo)
	{
		if (active)
		{
			switch(type)
			{
			case ObstaclesType::drop:
				if (txtrOne.width != rec.width || txtrOne.height != rec.height)
				{
					txtrOne.width = rec.width;
					txtrOne.height = rec.height;
				}
				DrawTexture(txtrOne, rec.x, rec.y, WHITE);
				break;
			case ObstaclesType::laser:
				if (txtrTwo.width != rec.width || txtrTwo.height != rec.height)
				{
					txtrTwo.width = rec.width;
					txtrTwo.height = rec.height;
				}
				DrawTexture(txtrTwo, rec.x, rec.y, WHITE);
				break;
			}			
		}
	}	
}
#ifndef PLAYER_H

#define PLAYER_H

#include "raylib.h"
#include "scenes.h"

namespace ENDLESS
{
	enum class LanePosition { up, middle, down };

	class Player
	{
	private:
		Rectangle rec;
		LanePosition currentLanePos;
		LanePosition lastLanePos;
		Color color;
		short moveUpKey;
		short moveDownKey;
		short jumpKey;
		short pauseKey;		
		const float gravity = 150.0f;
		float jumpingForce;
		float jumpHeight;
		float currentLaneYPos;
		float moveSpeed;
		bool jumping;
		bool falling;
		bool moving;
		Vector2 textScorePos;
		float textScoreFontSize;
		Color textScoreColor;
		static const short maxScore = 9;
		char score[maxScore];
		Vector2 scorePos;
		float scoreFontSize;
		Color scoreColor;
		float timer;
		short lifes;
	public:
		Player();
		void AddScore(short index, short value);
		void SetLifes(short _lifes);
		Rectangle GetRec();
		float GetY();
		float GetX();
		float GetWidth();
		float GetJumpHeight();
		short GetLifes();
		void Input(Scenes* scene);
		void Update(float firstLanePos, float secondLanePos, float thirthLanePos);
		void Draw(Texture2D sprite);
		void DrawScore();
		void SetCurrentLaneY(float value);
		void Move(float firstLanePos, float secondLanePos, float thirthLanePos);
		void VerticalJump();		
		void ScoreUp();
		void GameOver(Scenes* scene);
		void Reset();
		void DrawLifes(Texture2D lifesTexture);
	};
}
#endif

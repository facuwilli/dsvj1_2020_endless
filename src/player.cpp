#include "player.h"
namespace ENDLESS
{
	Player::Player()
	{
		rec.x = (GetScreenWidth() * 5) / 100;
		rec.y = 0;
		rec.width = (GetScreenWidth() * 3) / 100;
		rec.height = ((GetScreenHeight() * 10) / 100) / 2;
		currentLanePos = LanePosition::middle;
		lastLanePos = LanePosition::middle;
		color = GREEN;
		moveUpKey = KEY_UP;
		moveDownKey = KEY_DOWN;
		jumpKey = KEY_SPACE;
		pauseKey = KEY_P;		
		jumpingForce = 250;
		jumpHeight = 85;
		currentLaneYPos = 0;
		moveSpeed = 200;
		jumping = false;
		falling = false;
		moving = false;
		textScoreFontSize = GetScreenHeight() / 20;
		textScorePos = { static_cast<float>(GetScreenWidth() / 2) - static_cast<float>(textScoreFontSize * 2.5), static_cast<float>(GetScreenHeight() / 10) };
		textScoreColor = GREEN;
		for (short i = 0; i < maxScore; i++)
		{
			score[i] = 48;
		}
		scoreFontSize = GetScreenHeight() / 20;
		scorePos = { textScorePos.x + (GetScreenWidth() / 10), static_cast<float>(GetScreenHeight() / 10) };
		scoreColor = GREEN;
		timer = 0;
		lifes = 3;
	}
	void Player::AddScore(short index, short value)
	{
		if(!moving && !falling)
		{
			if (score[index] + value > 57)
			{
				score[index] += value;

				for (short i = maxScore - 1; i >= 0; i--)
				{
					if (score[i] > 57)
					{
						score[i] = 48;
						score[i - 1] += 1;
					}
				}
			}
			else
			{
				score[index] += value;
			}
		}		
	}
	void Player::SetLifes(short _lifes)
	{
		lifes = _lifes;
	}
	Rectangle Player::GetRec()
	{
		return rec;
	}
	float Player::GetY()
	{
		return rec.y;
	}
	float Player::GetX()
	{
		return rec.x;
	}
	float Player::GetWidth() 
	{
		return rec.width;
	}
	float Player::GetJumpHeight()
	{
		return jumpHeight;
	}
	short Player::GetLifes()
	{
		return lifes;
	}
	void Player::Input(Scenes* scene)
	{
		if(IsKeyPressed(pauseKey))
		{
			scene->ChangeCurrentScene(MainScenes::pause);
		}
		else if (!jumping && !falling && !moving)
		{
			if (IsKeyPressed(moveUpKey))
			{
				moving = true;

				switch (currentLanePos)
				{
				case LanePosition::middle:
					lastLanePos = LanePosition::middle;
					currentLanePos = LanePosition::up;
					break;
				case LanePosition::down:
					lastLanePos = LanePosition::down;
					currentLanePos = LanePosition::middle;
					break;
				}
			}
			else if (IsKeyPressed(moveDownKey))
			{
				moving = true;

				switch (currentLanePos)
				{
				case LanePosition::up:
					lastLanePos = LanePosition::up;
					currentLanePos = LanePosition::middle;
					break;
				case LanePosition::middle:
					lastLanePos = LanePosition::middle;
					currentLanePos = LanePosition::down;
					break;
				}
			}
			else if (IsKeyPressed(jumpKey))
			{
				jumping = true;
			}
		}	
	}
	void Player::Update(float firstLanePos, float secondLanePos, float thirthLanePos)
	{
		if (!jumping && !falling)
		{
			Move(firstLanePos, secondLanePos, thirthLanePos);
		}
		else
		{
			VerticalJump();
		}

		ScoreUp();
	}
	void Player::Draw(Texture2D sprite)
	{
		if(sprite.width != rec.width || sprite.height != rec.height)
		{
			sprite.height = rec.height;
			sprite.width = rec.width;
		}
		DrawTexture(sprite, rec.x, rec.y, color);		
		DrawScore();
	}
	void Player::DrawScore()
	{
		DrawText("SCORE", textScorePos.x, textScorePos.y, textScoreFontSize, textScoreColor);
		DrawText(score, scorePos.x, scorePos.y, scoreFontSize, scoreColor);
	}
	void Player::SetCurrentLaneY(float value)
	{
		if (currentLaneYPos != value)
		{
			currentLaneYPos = value;
		}
	}
	void Player::Move(float firstLanePos, float secondLanePos, float thirthLanePos)
	{
		switch (currentLanePos)
		{
		case LanePosition::up:

			SetCurrentLaneY(firstLanePos);

			if (rec.y > currentLaneYPos)
			{
				rec.y -= GetFrameTime() * moveSpeed;
			}
			else
			{
				rec.y = firstLanePos;
				moving = false;
			}

			break;

		case LanePosition::middle:

			SetCurrentLaneY(secondLanePos);

			if (rec.y > currentLaneYPos && lastLanePos == LanePosition::down)
			{
				rec.y -= GetFrameTime() * moveSpeed;
			}
			else if (rec.y < currentLaneYPos && lastLanePos == LanePosition::up)
			{
				rec.y += GetFrameTime() * moveSpeed;
			}
			else
			{
				rec.y = secondLanePos;
				moving = false;
			}

			break;

		case LanePosition::down:

			SetCurrentLaneY(thirthLanePos);

			if (rec.y < currentLaneYPos)
			{
				rec.y += GetFrameTime() * moveSpeed;
			}
			else
			{
				rec.y = thirthLanePos;
				moving = false;
			}

			break;
		}
	}
	void Player::VerticalJump()
	{
		if (jumping)
		{
			if (rec.y > (currentLaneYPos - jumpHeight))
			{
				rec.y -= GetFrameTime() * jumpingForce;
			}
			else
			{
				jumping = false;
				falling = true;
			}
		}
		else if (falling)
		{
			if (rec.y < currentLaneYPos)
			{
				rec.y += GetFrameTime() * gravity;
			}
			else
			{
				falling = false;
			}
		}
	}
	void Player::ScoreUp()
	{
		timer += GetFrameTime();
		
		if (timer >= GetFrameTime() * 20)
		{
			for(short i = maxScore - 1; i >= 0; i--)
			{
				if (score[i] < 57)
				{
					score[i] += 1;
					i = -1;
				}
				else 
				{
					score[i] = 48;
				}
			}

			timer = 0;
		}
	}
	void Player::GameOver(Scenes* scene)
	{
		if(lifes <= 0)
		{
			scene->ChangeCurrentScene(MainScenes::gameOver);
		}
	}
	void Player::Reset()
	{
		currentLanePos = LanePosition::middle;
		jumping = false;
		falling = false;
		moving = false;
		timer = 0;
		lifes = 3;
		for (short i = 0; i < maxScore; i++)
		{
			score[i] = 48;
		}
	}
	void Player::DrawLifes(Texture2D lifesTexture)
	{
		for (short i = 1; i <= lifes; i++)
		{
			DrawTexture(lifesTexture, GetScreenWidth() - (i * 50), 40, WHITE);			
		}
	}
}
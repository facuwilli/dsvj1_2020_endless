#ifndef SPAWNER_H

#define SPAWNER_H
#include "obstacle.h"
#include "player.h"

namespace ENDLESS 
{
	class Spawner
	{
	private:
		float timer;
		short maxTime;
		short minTime;
		short newObstacleTime;
		float laneYPos;
		short index;
		static const short numObstacles = 50;
		Obstacle* obstacles[numObstacles];
		short lastLane;
		short currentLane;
		short speedTimer;
		short timeTillSpeedUp;
		short maxTimesSpeedUp;

	public:
		Spawner();
		~Spawner();
		void ObstaclesGenerator(float firstLaneY, float secondLaneY, float thirdLaneY);
		ObstaclesType ReturnRandomType();
		void SetUpObstacle(float laneYPos);
		void RandomLane();
		void Update(int limit, float firstLaneY, float secondLaneY, float thirdLaneY);
		void Draw(Texture2D txtrOne, Texture2D txtrTwo);
		void Colisiones(Player* player);
		void Reset();
		void SpeedUp();
	};
}
#endif

#ifndef STRUCT_LANE_H

#define STRUCT_LANE_H

#include "raylib.h"

struct Lane
{
	Rectangle rec;
	Color color;
};

#endif 

#include "lane.h"

namespace ENDLESS 
{
	Lane::Lane()
	{
		rec.x = 0;
		rec.y = 0;
		rec.width = 0;
		rec.height = 0;
		color = WHITE;
	}
	void Lane::setX(float x)
	{
		rec.x = x;
	}
	void Lane::setY(float y)
	{
		rec.y = y;
	}
	void Lane::setWidth(float width)
	{
		rec.width = width;
	}
	void Lane::setHeight(float height)
	{
		rec.height = height;
	}
	void Lane::setColor(Color _color)
	{
		color = _color;
	}
	float Lane::getHeight()
	{
		return rec.height;
	}
	float Lane::getY()
	{
		return rec.y;
	}
	void Lane::Draw()
	{
		DrawRectangle(rec.x, rec.y, rec.width, rec.height, color);
	}
}
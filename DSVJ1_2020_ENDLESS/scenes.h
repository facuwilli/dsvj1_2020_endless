#ifndef SCENES_H

#define SCENES_H

#include "raylib.h"

namespace ENDLESS
{
	class Scenes
	{
	private:
		enum class MainScenes { menu, game, config, exit };
		MainScenes currentScene;
		Color background;
	public:
		Scenes();
		void ChangeCurrentScene(MainScenes scene);
		void SetBackgroundColoe(Color color);
		Color GetBackgroundColor();
	};
}

#endif

#include "scenes.h"

namespace ENDLESS
{
	Scenes::Scenes()
	{
		currentScene = MainScenes::menu;
		background = WHITE;
	}
	void Scenes::ChangeCurrentScene(MainScenes scene)
	{
		currentScene = scene;
	}
	void Scenes::SetBackgroundColoe(Color color)
	{
		background = color;
	}
	Color Scenes::GetBackgroundColor()
	{
		return background;
	}
}